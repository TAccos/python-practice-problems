# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    #if is_sunny = false and is_workday = true, return "umbrella"
    if is_sunny == False and is_workday == True:
        return "umbrella"
    if is_sunny == True and is_workday == True:
        return "laptop"
    elif is_workday == False:
        return "surfboard"
    #if is_workday = true and is_sunny = true, return laptop
    #if is_workday = false, return "surfboard"

#test
is_workday = False
is_sunny = True
print(gear_for_day(is_workday, is_sunny))
#success
