# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    #need new string
    result = ""
    for letter in s:
        # for every letter in the string
        if letter not in result:
            #add it to the end of the result
            result = result + letter
            #return the result
    return result

#test
s = "Meeaak"
print(remove_duplicate_letters(s))
#success
