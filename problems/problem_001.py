# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
   #compare value 1 and value 2
   if value1 <= value2:
   #return the smaller number
        return value1
   else:
       return value2

value1 = 2
value2 = 4
print(minimum_value(value1, value2))
