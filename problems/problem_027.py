# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    max_value = max(values)
    if len(values) == 0:
        return None
    else:
        return max_value

values = (1, 8, 5, 7, 4)
print(max_in_list(values))
