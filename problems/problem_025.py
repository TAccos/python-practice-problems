# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if len(values) == 0:
        return None
    for num in values:
        return sum(values)


#test
values = (1, 2, 3, 4)
print(calculate_sum(values))
#success
