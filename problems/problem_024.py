# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    #calculate average of numbers
    #if list is empty, return none
    if len(values) == 0:
        return None
    sum = 0
    #for each item in list of values, add to sum
    for item in values:
        sum = sum + item
        #now return that value and divide by num of items
    return sum / len(values)


print(calculate_average([1, 2, 3, 4, 5]))
