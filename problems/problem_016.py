# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    #if x and y are within a range of 0 - 10
    if x and y in range(0, 10 + 1):
        return True
    else:
        return False

x = 2
y = 10
print(is_inside_bounds(x, y))
