# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    if len(values) == 0:
        return None
    sum = 0
    #calculate average of scores
    for item in values:
        sum = sum + item
    average = sum / len(values)
    #if average is greater or equal to 90, return A
    if average >= 90:
        return "A"
    #if average is >= 80 and is <= 90, return B
    elif average >= 80:
        return "B"
    #if average is >= 70 and is <= 80, return C
    elif average >= 70:
        return "C"
    #if >= 60 and <= 70, return D
    elif average >= 60:
        return "D"
    else:
        return "F"
    #else return F

values = (50, 55, 96, 100, 70)
print(calculate_grade(values))
