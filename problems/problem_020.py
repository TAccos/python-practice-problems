# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    #if range of index of attendees list is >= range of index of members list/2
    if len(attendees_list) >= len(members_list) // 2:
        return "YAAAAS"
    else:
        return "NOOOOO"


attendees_list = [1, 2, 3, 4]
members_list = [5, 6, 7, 8, 9, 10, 11, 12]

print(has_quorum(attendees_list, members_list))
